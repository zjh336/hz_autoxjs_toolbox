# 华仔AutoXJs工具箱

#### 介绍
[《华仔AutoJs工具箱》图色(控件)脚本的好帮手](https://www.zjh336.cn/?id=2109)

1、当前为AutoX.js适配版本，已经做了许多修改。

2、AutoX版本还不够稳定，指令发送频繁时可能会导致闪退。

3、真机环境下，后台运行app时，可能会存在截屏阻塞或者无权限的情况，需要切换到app页面。

4、首次启动APP，授予无障碍和悬浮窗权限后，启动脚本可能还会提示打开无障碍，重启无障碍即可(这很奇怪)。

5、启动APP前，必须后台开启电话权限，否则启动闪退（这也很奇怪）。

#### 注意事项

1、文本框设置值需要使用setText() 不能使用attr。

2、文本框读取值需要使用getText() 不能使用attr。

3、输入框读取值需要使用text()。

4、文本框setText必须使用字符串类型,从storage取值后必须使用String()转换。

5、文本框setText传入为undefined会报错，需要做值判定。

6、开关组件(Switch)setChecked必须使用boolean值，为空值 undefined会有异常，必须Boolean()转换。

7、没有全局控制台组件gloabconsole，去除日志显示部分，增加日志查看入口。

8、没有$setting，快速获取权限值，应该有java类读取替代。

9、storage的存取值为，undefined会报错，需要做值判定。

10、newWebSocket必须给eventThread:'this'参数，否则内部执行setInterval会空指针，找不到this对象。

11、没有images.stopScreenCapture停止截图权限。

12、无法解决images.requestScreenCapture多次调用报错问题,且tryCatch无法捕获,直接去除该代码。

13、无特征匹配等新api，使用时请注意，utils.js中暂未删除不适配api。

14、谷歌ocr有内置，已替换调用代码。

15、远程指令涉及截图的，需要新开线程执行立即开始点击代码。

16、打包时编译选项全部勾选，运行配置仅需要勾选无障碍服务和悬浮窗权限。

17、运行打包后的app需要手动设置电话权限。



#### 软件架构
(原项目为AutoJsPro9.2.14开发)现在基于AutoX.js改造适配

### 联系方式

QQ:806073526

QQ群:818757945

![输入图片说明](%E8%B5%9E%E8%B5%8F.png)

[![郑建华/华仔AutoXJs工具箱_Web端](https://gitee.com/zjh336/hz_autojs_toolbox_web/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/zjh336/hz_autojs_toolbox_web)